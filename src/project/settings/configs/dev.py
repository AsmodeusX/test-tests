from settings.configs.base import *

DEBUG = env("DEBUG")


DOMAIN = '127.0.0.1'

EMAIL_PORT = 587
EMAIL_USE_SSL = False
EMAIL_USE_TLS = True

ALLOWED_HOSTS = env("ALLOWED_HOSTS").split(' ')

DATABASES.update({
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': env("DB_NAME"),
        'USER': env("DB_USER"),
        'PASSWORD': env("DB_PASSWORD"),
        'PORT': env("DB_PORT"),
        'HOST': '127.0.0.1',
    }
})
