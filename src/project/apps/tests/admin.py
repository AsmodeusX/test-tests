from django.contrib import admin
from .models import Test, Question, Answer, Result
from adminsortable2.admin import SortableAdminMixin


class AnswerInlineAdmin(admin.StackedInline):
    model = Answer
    fieldsets = (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'text', 'is_correct',
            ),
        }),
    )

class QuestionInlineAdmin(admin.StackedInline):
    model = Question
    fieldsets = (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'text',
            ),
        }),
    )


@admin.register(Question)
class QuestionAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general'),
            'fields': (
                'text', 'test'
            ),
        }),
    )
    inlines = (AnswerInlineAdmin, )


@admin.register(Test)
class TestAdmin(admin.ModelAdmin):
    fieldsets = (
        (None, {
            'fields': (
                'title', 'description',
            ),
        }),
    )
    inlines = (QuestionInlineAdmin, )



@admin.register(Result)
class ResultsAdmin(admin.ModelAdmin):
    list_display = ('test', 'user', 'correct_answers', 'total_questions', 'percentage_correct_answers')

    def has_add_permission(self, *args, **kwargs) -> bool:
        return False
    
    def has_change_permission(self, *args, **kwargs) -> bool:
        return False
    
    def has_delete_permission(self, *args, **kwargs) -> bool:
        return False
    
