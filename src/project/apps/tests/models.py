from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User
from adminsortable.models import SortableMixin


class Test(models.Model):
    title = models.CharField(_('name'), max_length=64)
    description = models.TextField(_('link'), blank=True)

    class Meta:
        verbose_name = _('Test')
        verbose_name_plural = _('Tests')

    def __str__(self):
        return self.title
    
    
    def list_questions(self):
        questions = self.questions.all()
        list_questions = []

        for question in questions:
            list_questions.append({
                "id": question.id,
                "text": question.text,
                "answers": question.list_answers()
            })
            
        return list_questions


class Question(models.Model):
    text = models.CharField(_('text'), max_length=128)
    test = models.ForeignKey(Test, verbose_name=_('test'), related_name='questions', on_delete=models.CASCADE)

    class Meta:
        verbose_name = _('Question')
        verbose_name_plural = _('Questions')

    def __str__(self):
        return self.text
    
    def list_answers(self):
        answers = self.answers.all()
        list_anwers = []

        for answer in answers:
            list_anwers.append({
                "id": answer.id,
                "text": answer.text
            })

        return list_anwers


class Answer(SortableMixin):
    text = models.CharField(_('text'), max_length=128)
    is_correct = models.BooleanField(_('is correct'), default=False)
    question = models.ForeignKey(Question, verbose_name=_('question'), related_name='answers', on_delete=models.CASCADE)
    sort_order = models.PositiveIntegerField(default=0)

    class Meta:
        verbose_name = _('Answer')
        verbose_name_plural = _('Answers')
        ordering = ['sort_order', ]

    def __str__(self):
        return self.text


class Result(models.Model):
    test = models.CharField(_("test"), max_length=128)
    user = models.ForeignKey(User, verbose_name=_("user"), on_delete=models.CASCADE)
    date = models.DateTimeField(_('date'), auto_now=True)
    correct_answers = models.PositiveSmallIntegerField(_('correct answers'), default=0)
    total_questions = models.PositiveSmallIntegerField(_('total questions'), default=0)

    class Meta:
        verbose_name = _('Result')
        verbose_name_plural = _('Results')
        ordering = ['-date']

    def __str__(self):
        return f"{self.user.get_full_name() if self.user.first_name or self.user.last_name else self.user.username} | {self.test}"

    def percentage_correct_answers(self):
        return f"{round(self.correct_answers * 100 / self.total_questions, 2)}%"
    
    def normal_date(self):
        return self.date.strftime("%d %m %Y, %H:%M:%S")
    
    def str_result(self):
        return f"{self.test} от {self.normal_date()}, результат {self.correct_answers} из {self.total_questions} ({self.percentage_correct_answers()})"