from django.contrib.auth.models import User
from rest_framework import serializers
from tests.models import Test, Result


class TestsSerializer(serializers.ModelSerializer):

    class Meta:
        model = Test
        fields = ('id', 'title', 'description', 'list_questions')


class TestSerializer(serializers.ModelSerializer):

    class Meta:
        model = Test
        fields = ('title', 'description', )

    
    @staticmethod
    def questions(obj):
        return obj.questions.all()



class UsersSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('username', 'email', 'first_name', 'last_name', 'password')


class ResultSerializer(serializers.ModelSerializer):

    class Meta:
        model = Result
        fields = ('test', 'user', 'correct_answers', 'total_questions', )

