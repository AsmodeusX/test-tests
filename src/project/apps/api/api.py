from django.views.generic import View
from django.contrib.auth.models import User
from django.utils.translation  import ugettext_lazy as _
from django.core import serializers
from django.contrib.auth import authenticate, login, logout
from django.http import JsonResponse
from rest_framework.permissions import AllowAny
from rest_framework.decorators import action
from rest_framework import generics, status, viewsets
from rest_framework.response import Response
from rest_framework.authtoken.models import Token
from .serializers import TestsSerializer, TestSerializer, UsersSerializer, ResultSerializer
from tests.models import Test, Result


class TestsView(viewsets.ReadOnlyModelViewSet):
  queryset = Test.objects.all()
  serializer_class = TestsSerializer

  @action(detail=True, methods=['get',])
  def test(self, request, *args, **kwargs): 
    serializer = TestSerializer
    return Response(serializer.data)
  

class ResultView(viewsets.ModelViewSet):
  queryset = Result.objects.all()
  serializer_class = ResultSerializer

  # @action(detail=False, methods=['post',], url_path="/send-result/")
  def create(self, request, *args, **kwargs):
    # return super().create(request, *args, **kwargs)
    data = request.data
    user = self.request.user
    test_id = int(data['test_id'])
    test = Test.objects.get(id=test_id)
    questions = test.questions.all()
    sended_questions = data['questions']
    correct_answers = 0

    for question in questions:
      if str(question.id) in sended_questions:
        question_answers = question.answers.filter(is_correct=True).values_list("id", flat=True)
        sended_question = sended_questions[str(question.id)]
        sended_answers = sended_question

        if sended_answers == [str(a) for a in list(question_answers)]:
          correct_answers += 1
        # if len(question_answers) == sended_questions

    total_questions = len(questions.all())
    Result.objects.create(
      test=test.title,
      user=user,
      correct_answers=correct_answers,
      total_questions=total_questions
    ).save()
    return Response({
      "status": _("Отправлено")
    })

  # @action(detail=False, methods=['post',], url_path="/send-result/")
  def list(self, request, *args, **kwargs):
    # return super().create(request, *args, **kwargs)
    user = self.request.user
    
    results = Result.objects.filter(user_id=user.id)
    list_results = []

    for result in results:
      list_results.append({
        'test': result.test,
        'date': result.date,
        'correct_answers': result.correct_answers,
        'total_questions': result.total_questions,
        'percentage_correct_answers': result.percentage_correct_answers(),
        'str_result': result.str_result(),
      })

    return Response({
      "results": list_results,
    })
  

class UsersView(viewsets.ModelViewSet):
  queryset = User.objects.all()
  serializer_class = UsersSerializer
  permission_classes = [AllowAny, ]

  @action(detail=False, methods=['post',], url_path='register')
  def register(self, request):
    data = request.data

    try:
      user = User.objects.create_user(
        data['login'],
        data['email'],
        data['password'], 
        first_name=data['first_name'],
        last_name=data['last_name'],)
      
      token = Token.objects.create(user=user)
      
    except Exception as e:
          return Response({
          "status": str(e)
        }, status=status.HTTP_400_BAD_REQUEST)
    return Response({
      "status": _("User created"),
      "token": str(token),
    })
  
  @action(detail=False, methods=['post',], url_path='login')
  def login(self, request, *args,  **kwargs):
    data = request.data
    user = authenticate(username=data['login'], password=data['password'])
    if user is not None:
      if user.is_active:
        try:
          token = Token.objects.get(user=user).key
        except Exception as e:
          print(e )
          return Response({
            "status": _("Can't get auth token. Contact to Administrator for generate it")
          }, status=status.HTTP_400_BAD_REQUEST)
        # login(request, user)
        return Response({
          "status": _("Authenticated successfully"),
          "name": user.get_full_name() if user.first_name or user.last_name else user.get_username(),
          "token": token,
        })
      else:
        return Response({
          "status": _("Your account is disabled")
        }, status=status.HTTP_400_BAD_REQUEST)
            
    else:
      return Response({
        "status": _("Incorrect Login")
      }, status=status.HTTP_400_BAD_REQUEST)
  
  @action(detail=False, methods=['get',], url_path='logout')
  def logout(self, request, *args, **kwargs):
    logout(request=request)
    return Response({
      "status": _("User logouted")
    })