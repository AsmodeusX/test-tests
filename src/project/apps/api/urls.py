from django.urls import path
from rest_framework import routers  
from . import api

app_name = 'api'


router = routers.DefaultRouter()
router.register(r'tests', api.TestsView, basename="tests")
router.register(r'users', api.UsersView, basename="users",)
router.register(r'results', api.ResultView, basename="results",)

urlpatterns = router.urls
